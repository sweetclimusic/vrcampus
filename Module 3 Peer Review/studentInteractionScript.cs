﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using vrCampusCourseware;
using VRCampus;

public class studentInteractionScript : MonoBehaviour {

    [Header("Courseware")]
    [SerializeField]
    TrackingDisplay leftScreen;
    [SerializeField]
    Transform laserHolder;
    [SerializeField]
    Transform laserBeam;
    [SerializeField]
    Transform sphereObject;
    [SerializeField]
    int beamDistance = 50;
    [SerializeField]
    Reticle gunReticle;
    
    Dictionary<Collider, robotScript> robotScripts;
    LaserPistol laserPistolScript;
    private const string VRTrigger = "OCTouch.SecondaryTrigger.Squeeze";
    private const string MobileVRTrigger = "OCG.Trigger";
    private const float defaultAxisDeadZone = 0.019f;
    private void Start()
    {
        robotScripts = new Dictionary<Collider, robotScript>();
        //find all robot tags
        var gameObjects = GameObject.FindGameObjectsWithTag("robot");
        foreach (var item in gameObjects)
        {
            var collider = item.GetComponent<Collider>();
            var rs = item.GetComponentInParent<robotScript>();
            if (collider && rs)
            {
                robotScripts.Add(collider, rs);
            }
        }

        if (laserHolder)
        {
            laserPistolScript = laserHolder.GetComponentInParent<LaserPistol>();
        }
    }

    private void Update()
    {
        RaycastHit hit;

        //simple raycast
        if(Physics.Raycast(laserHolder.position, laserHolder.forward,out hit, beamDistance))
        {
            //use distance, point and normal from the raycast hit
            //sphereObject.transform.up = -hit.normal;
            sphereObject.position = hit.point;

            gunReticle.SetPosition(hit);
            //var scaleFactor = 1f;
            //if(hit.point.x >= 100 && hit.point.x <= 300)
            //{
            //    scaleFactor = 25f;
            //}
            //if (hit.point.x > 300)
            //{
            //    scaleFactor = 50f;
            //}
            ////update scale based on distance.
            //sphereObject.localScale *= scaleFactor;
            var localBeamOffset = laserBeam.localPosition;
            var beam = laserBeam.localScale;
            laserBeam.localScale = new Vector3 (beam.x, hit.distance, beam.z );
            //use the scale as the offset to reposition the beam start point.
            laserBeam.localPosition = new Vector3(localBeamOffset.x, localBeamOffset.y, laserBeam.localScale.z + laserBeam.localScale.y);
            var hitRobot = hit.collider.tag == "robot";
            gunReticle.SetOn(hitRobot);
            if (hitRobot)
            {
                //get robotScript in grandparent.
                var robotScript = robotScripts[hit.collider];
                var headShot = hit.collider.name == "balloon_lowL" || hit.collider.name ==  "balloon_lowR" ? true:false;

                if (headShot)
                {
                    robotScript.Highlight();
                }

                //For each button press or joystick movement set one of the following.
                //device name, bool isTPTouched, bool isTPPressed, float trigger, float tpHorizontal, float tpVertical
                //Trigger - Axis 3 - Joystick 3rd Axis
                if ( Mathf.Abs(Input.GetAxis(VRTrigger)) > defaultAxisDeadZone || Input.GetAxis(MobileVRTrigger) > defaultAxisDeadZone)
                {
                    //fire projectile.
                    
                    if (laserPistolScript)
                    {
                        laserPistolScript.fireRound();
                    }
                    //popHead of robot.
                    
                    if (headShot && !robotScript.isPopped())
                    {
                        robotScript.PopHead();
                    }

                }
            }

        }
    }


}
