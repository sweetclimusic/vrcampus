﻿using System;
using System.Collections;
using UnityEngine;
using vrCampusCourseware;
using UnityEngine.XR;
using System.Collections.Generic;

public class studentLocomotionScript : MonoBehaviour
{
    [Header("Courseware")]
    [SerializeField]
    private TrackingDisplay trackingScreen;

    [SerializeField]
    private Transform laserHolder;

    [SerializeField]
    private Transform laserBeam;

    [SerializeField]
    private Transform dotHolder;

    [SerializeField]
    private int teleportDistance = 11;

    [SerializeField]
    private Transform teleportMarkerDirection;

    [SerializeField]
    private Transform VRCameraReference;

    [SerializeField]
    private Transform VRCamera;

    [SerializeField]
    private GameObject faderImageCanvas;

    [SerializeField]
    private CanvasGroup fadeCanvasGroup;

    private LocomotionStateManager currentLocomotionState;
    [SerializeField]
    private float rotationRange = 1.0f;

    private float playerHeight;

    [SerializeField]
    private float teleportStartDelay = 0.1f;

    [SerializeField]
    private float fadeStartDelay = 0.1f;

    [SerializeField]
    private float fadeDisplayTime = 0.6f;

    [SerializeField]
    private ControllerHider controllerReference;
    private Transform gripper;
    [SerializeField]
    private Transform ballTransform;
    private Rigidbody ballRigidbody;
    private Transform ballParentTransform;
    [SerializeField]
    private float gripMinDistance;

    
    private Transform identitySpace;
    private Vector3 cursorWorldPositon;
    private float identitySpaceRotate;
    private Quaternion lastCursorRotation;
    private bool isGrabbing;
    private bool isThrowing;
    private Vector3[] sampledControllerPositions;
    [SerializeField]
    private int sampleControllerPositionSize = 4;
    private int sampleControllerIndex = 0;
    [SerializeField]
    private float velocitySpeed = 10f;

    private switchRooms.roomType curRoom = switchRooms.roomType.Study;
    private switchRooms roomSwitcher;
    private bool TakePicture = true;
    private XRNode mainTrackedController = XRNode.RightHand;

    private void OnEnable()
    {
        roomSwitcher = FindObjectOfType<switchRooms>();
        roomSwitcher.onRoomChanged += new switchRooms.RoomChangeEvent(SwitchRoom);
    }

    private void OnDisable()
    {
        roomSwitcher.onRoomChanged -= new switchRooms.RoomChangeEvent(SwitchRoom);
    }

    public void SwitchRoom(switchRooms.roomType type)
    {
        curRoom = type;
    }

    //reconfigure for your headset controller
    private const string VRTouchPadPush = "OCTouch.SecondaryThumbstick.Press";
    private const string VRTrigger = "OCTouch.SecondaryTrigger.Squeeze";
    private const string VRTouchPadX = "SecondaryThumbstick.Horizontal";
    private const string VRTouchPadY = "SecondaryThumbstick.Vertical";
    private const string VRGrip = "OCTouch.SecondaryGrip";
    const float maxTriggerValue = 1f;
    //percentage strength based on the trigger VRTrigger Max value of 1.
    const float throwReleaseStrength = 0.6f;
    const float peekGripStrength = 0.8f;
    const float ZeroPercent = 0.1f;
    const float DeadZone = 0.001f; //deadZone
    //var peekGripStrengthPrecentage = Mathf.Abs(gripStrength / maxTriggerValue) * 100f;
    //var percentage = Mathf.Abs(maxTriggerValue / 100) * 20f;
    //var throwReleaseStrength = Mathf.Abs(gripStrength - percentage / maxTriggerValue) * 100f;

    private void Start()
    {
        currentLocomotionState = new LocomotionStateManager()
        {
            CurrentState = LocomotionState.NEUTRAL
        };

        if (!laserHolder)
        {
            laserHolder = GameObject.Find("laserHolder").GetComponent<Transform>();
        }

        if (!VRCameraReference)
        {
            VRCameraReference = GameObject.Find("frameOfReference").transform;
        }
        if (!VRCamera)
        {
            VRCamera = GameObject.Find("vrCamera").transform;
        }
        if (!fadeCanvasGroup && faderImageCanvas)
        {
            fadeCanvasGroup = faderImageCanvas.GetComponent<CanvasGroup>();
        }
        playerHeight = VRCameraReference.position.y;
        cursorWorldPositon = new Vector3(teleportMarkerDirection.position.x, playerHeight, teleportMarkerDirection.position.y);
        if (controllerReference)
        {
            gripper =  controllerReference.GetComponentInChildren<Transform>().Find("Gripper");
        }
        if (ballTransform)
        {
            ballRigidbody = ballTransform.GetComponent<Rigidbody>();
            ballParentTransform = ballTransform.parent;
        }
        sampledControllerPositions = new Vector3[sampleControllerPositionSize];
    }

    private void Update()
    {
       
        //can teleport when room is  locomotion
        if ( curRoom != switchRooms.roomType.Locomotion )
        {
            return;
        }
        //check if hitting floor before teleporting

        CheckPlayerNearInteractable();
        
        if (currentLocomotionState.CurrentState == LocomotionState.GRIPPER)
        {
            UpdateGripperInput();
            
        }
        else if(currentLocomotionState.CurrentState == LocomotionState.NEUTRAL || currentLocomotionState.CurrentState == LocomotionState.MARKER)
        {
            lastCursorRotation = teleportMarkerDirection.rotation;
            var Horizontal = UpdateLocomotionInput();
            UpdateLocomotion(Horizontal);
        }

        //trackingScreen.DebugInfo(
        //   ballTransform.position.ToString() + "\n" +
        //   ballTransform.rotation.ToString() + "\n" +
        //   throwVelocity.ToString() + "\n" +
        //   ballRigidbody.velocity.ToString() + "\n" +
        //   "Throwing " +isThrowing.ToString() + "\n" +
        //   "Gabbing: " + isGrabbing.ToString() + "\n" +
        //   "isKinematic " + ballRigidbody.isKinematic.ToString()


        //   );
    }
    private void FixedUpdate()
    {

        UpdateInteractions();
        if (isGrabbing)
        {
            List<XRNodeState> nodeStates = new List<XRNodeState>();
            InputTracking.GetNodeStates(nodeStates);
            var trackedPosition = Vector3.zero;
            var angularVelocity = Vector3.zero;
            for (int i = nodeStates.Count - 1; i >= 0; i--)
            {
                //limitation, only tracking right hand? need setting for user to transfer hands.
                //but on room space vr both hands are equal.

                //First use the node state and the more accurate TryGet Methods.
                //if any of the TryGetPosition or TGRotation return false 
                //fall back to the InputTracking.getPosition and IT.getRotation
                // took out check for left handers || nodeStates[i].nodeType == XRNode.LeftHand
                if (nodeStates[i].nodeType == mainTrackedController)
                {
                    //we are tracking a controller
                    if (!nodeStates[i].TryGetPosition(out trackedPosition))
                    {
                        trackedPosition = gripper.position;
                    }
                    //track the angular velocity
                    //Get some angular velocity as well
                    if (!nodeStates[i].TryGetAngularVelocity(out angularVelocity))
                        angularVelocity = Vector3.zero;
                    //rigidbody.angularVelocity = angularVelocity;
                    break;
                }
            }
            //gather a average of last controller positions.
            //NOTE! can't use local wasn't moving at all when using local.
            AddControllerPositionSample(trackedPosition);
            ballRigidbody.angularVelocity = angularVelocity;
            

        }
    }

    private void UpdateGripperInput()
    {

        if (currentLocomotionState.CurrentState == LocomotionState.TELEPORTING)
        {
            return;
        }
        //Will be using a percentage range for throwing based on a blog post I read about IRL throwing in VR.
        //http://bigblueboo.com/blog/how-to-make-throwing-in-vr-better/
        //TLDR  track the position for velocity calculation because a person know the weight of the controller
        //Use a % indicator for the controller to do actions. ie. start throwing at 20%. players never grip at 100% strenght.
        //**********NOTE writes research with vive wands. do you own
        //capture 4 frames of position input to cancel out noise that can occur for just the lastPosition and currentPosition
        var gripStrength = Input.GetAxis(VRTrigger);
       
        
        //Grabbed
        if (gripStrength >= peekGripStrength)
        {
            isGrabbing = true;
            isThrowing = false;
        }
        //Throwing
        if (gripStrength > ZeroPercent && gripStrength <= throwReleaseStrength && isGrabbing)
        {
            isGrabbing = false;
            isThrowing = true;
            
        }
        //Dropped
        if (gripStrength <= ZeroPercent)
        {
            isGrabbing = false;
            isThrowing = false;
        }
        
    }

    private void UpdateInteractions()
    {
        if (isThrowing)
        {
            //apply Calculated Velocity
            ballRigidbody.velocity = AverageVelocityDirection() * velocitySpeed;
            ballTransform.parent = ballParentTransform;
            ballRigidbody.isKinematic = false;
            isThrowing = false;
            isGrabbing = false;
            //start fresh position tracking
            sampledControllerPositions = new Vector3[sampleControllerPositionSize];
            //unity now applies the correct type of deltaTime based on which update loop you are in
            if (TakePicture)
            {
                trackingScreen.TakeScreenshot("throwBall");
                TakePicture = false;
            }
        }
        else if(isGrabbing)
        {
                ballRigidbody.isKinematic = true;
                ballTransform.parent = gripper;
                ballTransform.localPosition = controllerReference.GetGripperOffset();
                ballTransform.localPosition = controllerReference.GetGripperOffset();
        }
        else
        {
            //start fresh position tracking
            sampledControllerPositions = new Vector3[sampleControllerPositionSize];
        }
        
        
    }

    private void UpdateLocomotion(float Horizontal)
    {
        RaycastHit hit;
        //simple raycast, restricted by distance.
        if (Physics.Raycast(laserHolder.position, laserHolder.forward, out hit, teleportDistance) )
        {
            var localBeamOffset = laserBeam.localPosition;
            var beam = laserBeam.localScale;
            var offsetScale = hit.distance / 2;
            laserBeam.localScale = new Vector3(beam.x, offsetScale, beam.z);
            //use the scale as the offset to reposition the beam start point.
            laserBeam.localPosition = new Vector3(localBeamOffset.x, localBeamOffset.y, offsetScale);
            //use distance, point and normal from the raycast hit
            dotHolder.transform.up = -hit.normal;
            dotHolder.position = hit.point;

            ////update scale based on distance.
            //dotHolder.localScale = originalScale * teleportDistance;

            if (hit.collider.tag == "floor")
            {   
                if(currentLocomotionState.CurrentState == LocomotionState.TELEPORTING)
                {
                    StartCoroutine(DisplayFaderCanvas());
                }
                //rotate on the Z axis in localSpace.
                else if (currentLocomotionState.CurrentState == LocomotionState.MARKER && Mathf.Abs(Horizontal) > 0.0f)
                {
                    trackingScreen.InteractionInfo(hit);
                    cursorWorldPositon = new Vector3(hit.point.x, playerHeight, hit.point.z);
                    teleportMarkerDirection.position = Vector3.ProjectOnPlane(hit.point, hit.normal);
                    teleportMarkerDirection.Rotate(Vector3.up, Horizontal, Space.World);
                    identitySpaceRotate += Horizontal;
                    //fine the angle of new rotation.
                    var angle = Quaternion.Angle(teleportMarkerDirection.rotation, lastCursorRotation);
                    trackingScreen.LocomotionTPInfo(teleportMarkerDirection.position, angle);
                }
            }
            else
            {
                currentLocomotionState.Neutral();
            }
        
        }
    }

    /// <summary>
    /// Use PhysicsOverLap sphere or sqrt distance to find interactive object
    /// </summary>
    private void CheckPlayerNearInteractable(bool useSphereCollision = true)
    {
        var colliders = Physics.OverlapSphere(laserHolder.position, 0.2f);
        var sqrDistance = (laserHolder.position - ballTransform.position).sqrMagnitude;
        var sqrMinDistance = gripMinDistance * gripMinDistance;
        var nearObject = false;
        if (useSphereCollision)
        {
            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].transform == ballTransform)
                {
                    nearObject = true;
                    break;
                }
            }
        }
        else{
            if (sqrDistance < sqrMinDistance)
            {
                nearObject = true;
            }
        }
        if (nearObject)
        {
                controllerReference.ShowGripper(true);
                currentLocomotionState.Gripper();
        }
        else
        {
            controllerReference.ShowGripper(false);
            currentLocomotionState.Neutral();
        }
    }

    private float UpdateLocomotionInput()
    {
        if (currentLocomotionState.CurrentState == LocomotionState.TELEPORTING)
        {
            return 0f;
        }
        if (Input.GetAxis(VRTrigger) > ZeroPercent)
        {
            currentLocomotionState.Teleporting(); //isTeleporting
            
            //do right away
            return 0f;
        }
        //get user input
        var Horizontal = Input.GetAxis(VRTouchPadX) * rotationRange * Time.deltaTime;
        //locomation stateUpdate
        if (Input.GetButton(VRTouchPadPush))
        {
            currentLocomotionState.Marking();
        }
        if (Input.GetButtonUp(VRTouchPadPush))
        {
            ResetTargetPosition(VRCameraReference);
        }
        return Horizontal;
    }

    private IEnumerator DisplayFaderCanvas()
    {
        yield return new WaitForSeconds(fadeStartDelay);
        faderImageCanvas.SetActive(true);
        //Fade transparent to black image.
        var start = 0f;
        var end = 1f;
        var fadeTimer = 0.001f;
        while (fadeTimer <= fadeDisplayTime)
        {
            fadeCanvasGroup.alpha += Mathf.Lerp(start, end, fadeTimer / fadeDisplayTime);
            fadeTimer += Time.deltaTime;
            yield return null;
        }
        //make sure it's full black.
        fadeCanvasGroup.alpha = 1f;
        yield return StartCoroutine(TeleportSafely());
    }

    private void ResetTargetPosition(Transform refObject)
    {
        var referencePosition = refObject.position;
        currentLocomotionState.Neutral();
        teleportMarkerDirection.position = new Vector3(referencePosition.x, 0.0f, referencePosition.z);
        teleportMarkerDirection.rotation = lastCursorRotation;
        cursorWorldPositon = new Vector3(referencePosition.x, playerHeight, referencePosition.z);
        identitySpaceRotate = 0f;
    }

    private IEnumerator TeleportSafely()
    {
        //Look at the last recorde location in world space.
        VRCameraReference.position = cursorWorldPositon;
        //now find new forward rotation.
        UpdateRotation();
        yield return new WaitForSeconds(teleportStartDelay);
        var start = 1f;
        var end = 0f;
        //Fade transparent Image to black image
        var fadeTimer = 0.001f;
        while (fadeTimer <= fadeDisplayTime)
        {
            fadeCanvasGroup.alpha += Mathf.Lerp(start, end, fadeTimer / fadeDisplayTime);
            fadeTimer += Time.deltaTime;
            yield return null;
        }
        //zero out and disable fader image
        faderImageCanvas.SetActive(false);
        ResetTargetPosition(teleportMarkerDirection);
        
    }

    /// <summary>
    /// Takes the rotation angle of the cursor and rotates the frame of reference by the angle of the rotation.
    /// </summary>
    private void UpdateRotation()
    {
        VRCameraReference.Rotate(Vector3.up, identitySpaceRotate, Space.World);
    }

    /// <summary>
    /// Manage the Vector3 array sampledControllerPosition. 
    /// resets the sampleControllerIndex when it excedes the sampleControllerPositionSize
    /// </summary>
    /// <param name="newSamplePosition"></param>
    private void AddControllerPositionSample(Vector3 newSamplePosition)
    {
        sampledControllerPositions[sampleControllerIndex] = newSamplePosition;
        if((sampleControllerIndex + 1) < sampleControllerPositionSize)
        {
            sampleControllerIndex++;
        }
        else
        {
            sampleControllerIndex = 0;
        }
        
    }
    /// <summary>
    /// returns the average of all vectors in sampledControllerPositions to represent
    /// a near closets user interpetation of a throw.
    /// </summary>
    /// <returns>Vector3 average</returns>
    private Vector3 AverageVelocityDirection()
    {
        var average = Vector3.zero;
        for (int i = 0; i < sampledControllerPositions.Length -1; i++)
        {
            //total are CURRENT positions
             average += sampledControllerPositions[i] - sampledControllerPositions[i + 1];
        }
        return average /= sampledControllerPositions.Length;
    }
}

internal enum LocomotionState
{
    NEUTRAL = 0, MARKER = 1, TELEPORTING = 2, GRIPPER = 3
};

internal struct LocomotionStateManager
{
    private LocomotionState currentState;

    public void SwitchStates(LocomotionState newState = LocomotionState.NEUTRAL)
    {
        CurrentState = newState;
    }

    public void Marking()
    {
        SwitchStates(LocomotionState.MARKER);
    }

    public void Neutral()
    {
        SwitchStates();
    }

    public void Teleporting()
    {
        SwitchStates(LocomotionState.TELEPORTING);
    }

    public void Gripper()
    {
        SwitchStates(LocomotionState.GRIPPER);
    }

    public LocomotionState CurrentState
    {
        get
        {
            return currentState;
        }

        set
        {
            currentState = value;
        }
    }
}