﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using vrCampusCourseware;

namespace VRCampus
{
    public class studentHomeScript : MonoBehaviour
    {
        [Header("Courseware")]
        [SerializeField]
        TrackingDisplay leftScreen;

        [SerializeField] private Transform vrCamera;
        [SerializeField] private float gazeLength = 50f;
        [SerializeField] private Reticle reticle;
        private gazeableObject currentGazeObject;
        public void Awake()
        {
            //grab the scene main camera transform for later calculations
            vrCamera = Camera.main.transform;
            if(reticle == null)
            {
                Debug.Log("reticle required");
            }
        }

        public void switchRoom(switchRooms.roomType rt)
        {
            
        }

        void Update()
        {
            CheckGaze();
        }
        private void DeactiveateLastInteractible()
        {
            if (currentGazeObject == null)
                return;

            currentGazeObject.Out();
            currentGazeObject = null;
            reticle.SetOn(false);
            reticle.SetPosition();
        }
        private void CheckGaze()
        {
            var ray = new Ray(vrCamera.position, vrCamera.forward);
            //test hit
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, gazeLength))
            {
                var interactableObject = hit.collider.GetComponent<gazeableObject>();
                reticle.SetPosition(hit);
                reticle.SetOn(false);
                if (interactableObject)
                {
                    //update reticle of hit
                    reticle.SetOn(true);
                    
                    //continue is interactable.
                    if (currentGazeObject != interactableObject)
                    {
                        currentGazeObject = interactableObject;
                        interactableObject.Over();
                    }
                    return;
                }
            }
            DeactiveateLastInteractible();
        }
    }
}
