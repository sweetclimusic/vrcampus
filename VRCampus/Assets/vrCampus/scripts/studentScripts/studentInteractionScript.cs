﻿using System.Collections.Generic;
using UnityEngine;
using VRCampus;
using vrCampusCourseware;

public class studentInteractionScript : MonoBehaviour
{
    [Header("Courseware")]
    [SerializeField]
    private TrackingDisplay leftScreen;

    [SerializeField]
    private Transform laserHolder;

    [SerializeField]
    private Transform laserBeam;

    [SerializeField]
    private Transform sphereObject;

    [SerializeField]
    private int beamDistance = 50;

    [SerializeField]
    private Reticle gunReticle;

    private Dictionary<Collider, robotScript> robotScripts;
    private LaserPistol laserPistolScript;
    private const string VRTrigger = "OCTouch.SecondaryTrigger.Squeeze";
    private const string MobileVRTrigger = "OCG.Trigger";
    private const float defaultAxisDeadZone = 0.019f;

    private switchRooms.roomType curRoom = switchRooms.roomType.Study;
    private switchRooms roomSwitcher;

    private void OnEnable()
    {
        roomSwitcher = FindObjectOfType<switchRooms>();
        roomSwitcher.onRoomChanged += new switchRooms.RoomChangeEvent(SwitchRoom);
    }

    private void OnDisable()
    {
        roomSwitcher.onRoomChanged -= new switchRooms.RoomChangeEvent(SwitchRoom);
    }

    public void SwitchRoom(switchRooms.roomType type)
    {
        curRoom = type;
    }

    private void Start()
    {
        robotScripts = new Dictionary<Collider, robotScript>();
        //find all robot tags
        var gameObjects = GameObject.FindGameObjectsWithTag("robot");
        foreach (var item in gameObjects)
        {
            var collider = item.GetComponent<Collider>();
            var rs = item.GetComponentInParent<robotScript>();
            if (collider && rs)
            {
                robotScripts.Add(collider, rs);
            }
        }

        if (laserHolder)
        {
            laserPistolScript = laserHolder.GetComponentInParent<LaserPistol>();
        }
    }

    private void Update()
    {
        if (curRoom != switchRooms.roomType.Interaction)
        {
            return;
        }
        RaycastHit hit;

        //simple raycast
        if (Physics.Raycast(laserHolder.position, laserHolder.forward, out hit, beamDistance))
        {
            //use distance, point and normal from the raycast hit
            //sphereObject.transform.up = -hit.normal;
            sphereObject.position = hit.point;

            gunReticle.SetPosition(hit);
            //var scaleFactor = 1f;
            //if(hit.point.x >= 100 && hit.point.x <= 300)
            //{
            //    scaleFactor = 25f;
            //}
            //if (hit.point.x > 300)
            //{
            //    scaleFactor = 50f;
            //}
            ////update scale based on distance.
            //sphereObject.localScale *= scaleFactor;
            var localBeamOffset = laserBeam.localPosition;
            var beam = laserBeam.localScale;
            var offsetScale = hit.distance / 2;
            laserBeam.localScale = new Vector3(beam.x, offsetScale, beam.z);
            //use the scale as the offset to reposition the beam start point.
            laserBeam.localPosition = new Vector3(localBeamOffset.x, localBeamOffset.y, offsetScale);
            var hitRobot = hit.collider.tag == "robot";
            gunReticle.SetOn(hitRobot);
            if (hitRobot)
            {
                //get robotScript in grandparent.
                var robotScript = robotScripts[hit.collider];
                var headShot = hit.collider.name == "balloon_lowL" || hit.collider.name == "balloon_lowR" ? true : false;

                if (headShot)
                {
                    robotScript.Highlight();
                }

                //For each button press or joystick movement set one of the following.
                //device name, bool isTPTouched, bool isTPPressed, float trigger, float tpHorizontal, float tpVertical
                //Trigger - Axis 3 - Joystick 3rd Axis
                if (Mathf.Abs(Input.GetAxis(VRTrigger)) > defaultAxisDeadZone || Input.GetAxis(MobileVRTrigger) > defaultAxisDeadZone)
                {
                    //fire projectile.

                    if (laserPistolScript)
                    {
                        laserPistolScript.fireRound();
                    }
                    //popHead of robot.

                    if (headShot && !robotScript.isPopped())
                    {
                        robotScript.PopHead();
                    }
                }
            }
        }
    }
}