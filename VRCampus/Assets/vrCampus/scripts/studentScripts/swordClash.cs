﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class swordClash : MonoBehaviour {

    [SerializeField]
    AudioClip collideAudio;
    [SerializeField]
    AudioSource swordSource;
    [SerializeField]
    ParticleSystem swordHit;
    private void Start()
    {
        if(collideAudio && swordSource)
        {
            swordSource.clip = collideAudio;
        }
    }
    // Use this for initialization
    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("collided");
        StartCoroutine(PlayImpact());
    }

    IEnumerator PlayImpact()
    {
        swordSource.Play();
        swordHit.Play();
        yield return null;
    }
}
