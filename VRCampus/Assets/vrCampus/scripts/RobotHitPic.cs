﻿using System;
using UnityEngine;
using vrCampusCourseware;

namespace VRCampus
{
    [RequireComponent( typeof(Rigidbody))]
    public class RobotHitPic : MonoBehaviour
    {
        public event Action OnHit;
        // Use this for initialization
        void OnTriggerEnter(Collider other)
        {
            if (other.tag != "robot") return;
            var rs = other.gameObject.GetComponentInParent<robotScript>();
            if (OnHit != null)
            {
                OnHit();
                if (rs)
                {
                    rs.PopHead();
                    rs.Highlight();
                }
            }
        }
    }
}