﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class BillboardScript : MonoBehaviour {

    [SerializeField]
    Camera vrCamera;
    Vector3 target;
    List<XRNodeState> nodeStates;
    
    // Use this for initialization
    void Start () {
        if (!vrCamera)
        {
            vrCamera = Camera.current;
        }
        nodeStates = new List<XRNodeState>();
    }
    private void Update()
    {
        target = vrCamera.transform.position;
        InputTracking.GetNodeStates(nodeStates);
        foreach (var item in nodeStates)
        {
            if (item.nodeType == XRNode.Head)
            {
                if(!item.TryGetPosition(out target))
                {
                    target = InputTracking.GetLocalPosition(XRNode.Head);
                }
                break;
            }
        }
        transform.LookAt(target);
    }
}
