﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GazeTextScaler : MonoBehaviour {
    private gazeableObject interactiveTextItem;
    [Tooltip("Local Scale"),SerializeField]
    private float scalefactor;
    private void Awake()
    {
        interactiveTextItem = GetComponent<gazeableObject>();
    }
    private void OnEnable()
    {
        interactiveTextItem.OnOver += HandleOver;
        interactiveTextItem.OnOut += HandleOut;
    }

    private void HandleOut()
    {
        if (scalefactor > 0f)
            this.transform.localScale = Vector3.one * scalefactor;
    }

    private void HandleOver()
    {
        //set back original scale
        if (scalefactor > 0f)
            this.transform.localScale = Vector3.one;
    }

    private void OnDisable()
    {
        interactiveTextItem.OnOver -= HandleOver;
        interactiveTextItem.OnOut -= HandleOut;
    }
    // Use this for initialization
    void Start () {
        
    }
    
    // Update is called once per frame
    void Update () {
        
    }
}
