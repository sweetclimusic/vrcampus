﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetBall : MonoBehaviour {

    [SerializeField]
    Transform parentReset;

    private void OnTriggerEnter(Collider other)
    {
        if(other.name == "ball")
        {
            other.transform.parent = parentReset;
            other.transform.position = Vector3.zero;
        }
    }
}
