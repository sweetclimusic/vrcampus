﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using vrCampusCourseware;

namespace VRCampus
{
    public class studentVRUXScript : MonoBehaviour
    {

        [Header("Courseware")]
        [SerializeField]
        TrackingDisplay trackingScreen;
        [SerializeField]
        private Transform laserHolder;

        [SerializeField]
        private Transform laserBeam;

        [SerializeField]
        private Transform dotHolder;

        private int uiPointerDistance = 150;

        private switchRooms.roomType curRoom = switchRooms.roomType.Study;
        private switchRooms roomSwitcher;

        private ButtonInjector button;
        //Screenshots to take.
        private bool vrGrabCapture = true;
        private bool vrPunchCapture = true;

        private bool vrUXCaptureH = true;
        private bool vrUXCaptureP = true;
        private bool vrUXInfoScreen = true;
        private bool buttonHeld;

        [SerializeField]
        private float offset = 4f;
        [SerializeField]
        private Transform mobileVRController;
        [SerializeField]
        private Transform glove;
        [SerializeField]
        private BoxingGlove boxingGlove;
        [SerializeField]
        private PhysicsTool physicsTool;
        [SerializeField]
        private RobotHitPic robotHit;
        private Transform gloveStart;
        private float gloveTotalRotation;
        private float elevateSpeed = 0.2f;
        
        private bool ringDeployed;
        private bool ringGrabbed;
        
        [SerializeField]
        private Transform leftHandleTransform;
        [SerializeField]
        private Transform rightHandleTransform;
        [SerializeField]
        private float gripMinDistance = 2f;

        private Vector3[] sampledControllerPositions;
        private int sampleControllerPositionSize = 4;
        private int sampleControllerIndex = 0;
        private Vector3 lastControllerPosition;
        //change if you're left handed
        private XRNode mainTrackedController = XRNode.RightHand;
        private Vector3 finalHeight;
        private Quaternion lastRingRotation;

        //controllers
        private const string VRTouchPadPush = "OCTouch.SecondaryThumbstick.Press";
        private const string VRTrigger = "OCTouch.SecondaryTrigger.Squeeze";
        private void OnEnable()
        {
            roomSwitcher = FindObjectOfType<switchRooms>();
            roomSwitcher.onRoomChanged += new switchRooms.RoomChangeEvent(SwitchRoom);
            physicsTool.OnHit += PushButton;
            robotHit.OnHit += HitRobot;
        }

        private void HitRobot()
        {
            //unity now applies the correct type of deltaTime based on which update loop you are in
            if (vrPunchCapture)
            {
                trackingScreen.TakeScreenshot("vrPunchCapture");
                vrPunchCapture = false;
            }

        }

        private void OnDisable()
        {
            roomSwitcher.onRoomChanged -= new switchRooms.RoomChangeEvent(SwitchRoom);
            physicsTool.OnHit -= PushButton;
            robotHit.OnHit -= HitRobot;


        }

        public void SwitchRoom(switchRooms.roomType type)
        {
            curRoom = type;
        }

        private void Start()
        {
            if (glove)
            {
                gloveStart = glove.transform;
            }
            sampledControllerPositions = new Vector3[sampleControllerPositionSize];

        }

        private void Update()
        {
            if (curRoom != switchRooms.roomType.VRUX)
            {
                return;
            }
            if(Input.GetAxis(VRTrigger) < 0.001f)
            {
                ringGrabbed = false;
            }
            UpdateUIInteractions();
            //useSphereCollision default to true
            if (!ringGrabbed)
            {
                CheckControllerNearHandle();
            }

            UpdateRingPositionAndRotation();
            //debugging
            trackingScreen.DebugInfo(
               laserHolder.position.ToString() + "\n" +
               glove.position.ToString() + "\n" +
               glove.localPosition.ToString() + "\n" +
               (laserHolder.position - lastControllerPosition).ToString() + "\n" +
               new Vector3(glove.position.x, (laserHolder.position - lastControllerPosition).y, glove.position.z).ToString()
               );

        }
        private void FixedUpdate()
        {
            List<XRNodeState> nodeStates = new List<XRNodeState>();
            InputTracking.GetNodeStates(nodeStates);
            var trackedPosition = Vector3.zero;
            var angularVelocity = Vector3.zero;
            for (int i = nodeStates.Count - 1; i >= 0; i--)
            {
                //limitation, only tracking right hand? need setting for user to transfer hands.
                //but on room space vr both hands are equal.

                //First use the node state and the more accurate TryGet Methods.
                //if any of the TryGetPosition or TGRotation return false 
                //fall back to the InputTracking.getPosition and IT.getRotation
                // took out check for left handers || nodeStates[i].nodeType == XRNode.LeftHand
                if (nodeStates[i].nodeType == mainTrackedController)
                {
                    //we are tracking a controller
                    if (!nodeStates[i].TryGetPosition(out trackedPosition))
                    {
                        trackedPosition = InputTracking.GetLocalPosition(mainTrackedController);
                    }
                    //track the angular velocity
                    //Get some angular velocity as well
                    if (!nodeStates[i].TryGetAngularVelocity(out angularVelocity))
                        angularVelocity = Vector3.zero;
                    //rigidbody.angularVelocity = angularVelocity;
                    break;
                }
            }
            AddControllerPositionSample(new Vector3 (0,trackedPosition.y,0));
        }

        private void PushButton()
        {
            //boxingGlove display velocity info
            var velocity = AverageVelocityDirection();
            trackingScreen.VRUXHitInfo(velocity);
            //new unity converts Time.deltaTime to fixedDeltaTime when in fixed update
            boxingGlove.Extension = Mathf.Clamp(velocity.sqrMagnitude, 0, 1f);
        }

        private void CheckControllerNearHandle(bool useSphereCollision = true)
        {
            var colliders = Physics.OverlapSphere(laserHolder.position, 0.2f);
            var sqrLeftDistance = (laserHolder.position - leftHandleTransform.position).sqrMagnitude;
            var sqrRightDistance = (laserHolder.position - rightHandleTransform.position).sqrMagnitude;
            var sqrMinDistance = gripMinDistance * gripMinDistance;
            var nearObject = false;
            if (useSphereCollision)
            {
                for (int i = 0; i < colliders.Length; i++)
                {
                    if (colliders[i].transform == leftHandleTransform || colliders[i].transform == rightHandleTransform)
                    {
                        nearObject = true;
                        break;
                    }
                }
            }
            else 
            {
                //use Direct Distance checking.
                if (sqrLeftDistance < sqrMinDistance || sqrRightDistance < sqrMinDistance)
                {
                    nearObject = true;
                }
            }
            if (nearObject && Input.GetAxis(VRTrigger) > 0.001f)
            {
                ringGrabbed = true;
                if (vrGrabCapture)
                {
                    trackingScreen.TakeScreenshot("vrGrabCapture");
                    vrGrabCapture = false;
                }
            }
        }

        private void UpdateRingPositionAndRotation()
        {
            lastRingRotation = glove.rotation;
            lastControllerPosition = glove.position;
            if (ringDeployed && ringGrabbed)
            {
                //test1
                var YRot = -laserHolder.rotation.y * Time.deltaTime * 180f;
                glove.Rotate(Vector3.forward, YRot);
                var YPos = offset * laserHolder.position.y;
                glove.position = new Vector3(0f, offset * laserHolder.position.y);
                
            }



        }
        Quaternion ClampAngle(Quaternion q)
        {
            q.x /= q.w;
            q.y /= q.w;
            q.z /= q.w;
            q.w =1.0f;
            var angleX = 2.0f * Mathf.Rad2Deg * Mathf.Abs(q.x);
            angleX = Mathf.Clamp(angleX, -100f, 100f);
            q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);
            return q;
        }
        private void UpdateUIInteractions()
        {
            RaycastHit hit;
            if (Physics.Raycast(laserHolder.position, laserHolder.forward, out hit, uiPointerDistance))
            {
                var localBeamOffset = laserBeam.localPosition;
                var beam = laserBeam.localScale;
                var offsetScale = hit.distance / 2;
                laserBeam.localScale = new Vector3(beam.x, offsetScale, beam.z);
                //use the scale as the offset to reposition the beam start point.
                laserBeam.localPosition = new Vector3(localBeamOffset.x, localBeamOffset.y, offsetScale);
                
                var buttonInjector = hit.collider.GetComponent<ButtonInjector>();
                if (hit.collider.tag == "vrux" && buttonInjector)
                {
                    if (buttonHeld && buttonInjector != button)
                    {
                        //user looking at new button
                        ButtonExit();
                    }
                    button = buttonInjector;
                    button.OnEnter();
                    if (vrUXCaptureH)
                    {
                        trackingScreen.TakeScreenshot("vrUXCaptureH");
                        vrUXCaptureH = false;
                    }
                    //interact with the UI button.
                    trackingScreen.VRUXRayInfo(hit);
                    if (vrUXInfoScreen)
                    {
                        trackingScreen.TakeScreenshot("vrUXInfoScreen");
                        vrUXInfoScreen = false;
                    }
                    //apply actions on the button.
                    if (Input.GetButton(VRTouchPadPush))
                    {
                        button.OnPress();
                        buttonHeld = true;
                        if (vrUXCaptureP)
                        {
                            trackingScreen.TakeScreenshot("vrUXCaptureP");
                            vrUXCaptureP = false;
                        }

                    }
                    if (Input.GetButtonUp(VRTouchPadPush))
                    {
                        ButtonExit();
                        
                    }


                }
            }
        }

        void ButtonExit()
        {
            buttonHeld = false;
            button.OnRelease();
            button = null;
            
        }

        public void DeployRing()
        {
            if (ringDeployed)
            {
                StartCoroutine(Recall());
            }
            else
            {
                StartCoroutine(Deploy());
            }
            
        }
        IEnumerator Deploy()
        {
          boxingGlove.Extension = 0f;
          //updating Z due to local position and model rotated by -90
          finalHeight = new Vector3(gloveStart.position.x, offset, gloveStart.position.z);
            var t = 0f;
            while (t <= 1f)
            {
                t += Time.deltaTime * elevateSpeed;
                glove.position = Vector3.Lerp(gloveStart.position,finalHeight, t);
                yield return null;

            }
            ringDeployed = true;
        }
        IEnumerator Recall()
        {
            boxingGlove.Extension = 0f;
            glove.position = gloveStart.position;
            //reset ring
            ringDeployed = false;
            glove.rotation = gloveStart.rotation;
            gloveTotalRotation = 0f;
            yield return null;

        }
        /// <summary>
        /// Manage the Vector3 array sampledControllerPosition. 
        /// resets the sampleControllerIndex when it excedes the sampleControllerPositionSize
        /// </summary>
        /// <param name="newSamplePosition"></param>
        void AddControllerPositionSample(Vector3 newSamplePosition)
        {
            sampledControllerPositions[sampleControllerIndex] = newSamplePosition;
            if ((sampleControllerIndex + 1) < sampleControllerPositionSize)
            {
                sampleControllerIndex++;
            }
            else
            {
                sampleControllerIndex = 0;
            }

        }
        /// <summary>
        /// returns the average of all vectors in sampledControllerPositions to represent
        /// a near closets user interpetation of a throw.
        /// </summary>
        /// <returns>Vector3 average</returns>
        private Vector3 AverageVelocityDirection()
        {
            var average = Vector3.zero;
            for (int i = 0; i < sampledControllerPositions.Length - 1; i++)
            {
                //total are CURRENT positions
                average += sampledControllerPositions[i] - sampledControllerPositions[i + 1];
            }
            return average /= sampledControllerPositions.Length * Time.deltaTime;
        }

    }
}
