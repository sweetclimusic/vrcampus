﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.UI;
using vrCampusCourseware;
using System;

public class studentTrackingScript : MonoBehaviour {

    [Header("Courseware")]
    [SerializeField]
    TrackingDisplay trackingScreen;


    [SerializeField]
    float viweportScale = 0.7f;
    private List<XRNodeState> nodeStates;
    private float defaultAxisDeadZone = 0.001f;
  
    private float defaultStickDeadZone = 0.19f;
    
    [SerializeField]
    private Text fpsText;
    private float inputRange = 1f;
    private string mainController;

    private int framesCount;
    
    private float updateInterval = 0.5f;
    private float frameTime;
    #region unused
    private float lastFrameCount = 0f;
    private int inputFrameRate = 3;
    //something I've been thinking about, might as well input it here.
    private Queue<ControllerInput> inputBuffer;
    private int maxInputBufferSize = 5;
    #endregion

    //Robot variables
    [SerializeField]
    private Transform opponentSword;
    [SerializeField]
    private Transform opponentHead;
    [SerializeField]
    private Transform leftOpponentSword;
    [SerializeField]
    private Transform leftOpponentHead;

    #region global scope tracking variables
    Vector3 lastHeadPosition = Vector3.zero;
    Quaternion lastHeadRotation = Quaternion.identity;
    Vector3 lastControllerPosition = Vector3.zero;
    Quaternion lastControllerRotation = Quaternion.identity;
    #endregion
    private void Start()
    {
        XRSettings.renderViewportScale = viweportScale;
        nodeStates = new List<XRNodeState>();
        //get all notestats
        InputTracking.GetNodeStates(nodeStates);
        for(var i = 0; i < nodeStates.Count; i++)
        {
            //firsttime look at player
            if(nodeStates[i].nodeType == XRNode.Head && nodeStates[i].TryGetPosition(out lastHeadPosition))
            {
                leftOpponentHead.LookAt(lastHeadPosition);
                opponentHead.LookAt(lastHeadPosition);
                //end loop
                break;
            }
        }
        //so we don't lose any inputs.
        inputBuffer = new Queue<ControllerInput>(); //too slow to use
        if(fpsText == null)
        {
            fpsText = GameObject.Find("hudText").GetComponent<Text>();
        }

    }
    //drop input if greater the
    private void AddToInputBuffer(ControllerInput obj)
    {
        if(inputBuffer.Count > maxInputBufferSize - 1)
        {
            inputBuffer.Dequeue();
        }
        inputBuffer.Enqueue(obj);
    }
    private void RemoveFromInputBuffer()
    {
        //first in first out
        if(inputBuffer.Count <= 0)
        {
            return;
        }
        var controllerInput = inputBuffer.Dequeue();
        if (controllerInput.Equals(default(ControllerInput)))
        {
            return;
        }
        //send input to tracking screen
        trackingScreen.ControllerInfo(controllerInput.Device, controllerInput.Pressed, controllerInput.Touched,
            controllerInput.Trigger, controllerInput.Horizontal, controllerInput.Vertical);

    }
    //bind the InputTracking actions to the TrackingDisplay
    void OnEnable()
    {
        InputTracking.nodeAdded += InputTracking_nodeAdded;
        InputTracking.nodeRemoved += InputTracking_nodeRemoved;
        InputTracking.trackingAcquired += InputTracking_trackingAcquired;
        InputTracking.trackingLost += InputTracking_trackingLost;
    }

    void OnDisable()
    {
        InputTracking.nodeAdded -= InputTracking_nodeAdded;
        InputTracking.nodeRemoved -= InputTracking_nodeRemoved;
        InputTracking.trackingAcquired -= InputTracking_trackingAcquired;
        InputTracking.trackingLost -= InputTracking_trackingLost;
    }
    void InputTracking_trackingLost(XRNodeState obj) => trackingScreen.TrackingEvent(TrackingDisplay.TrackingEventType.trackingLost, obj);

    void InputTracking_trackingAcquired(XRNodeState obj) =>  trackingScreen.TrackingEvent(TrackingDisplay.TrackingEventType.trackingAcquired, obj);
    
    void InputTracking_nodeRemoved(XRNodeState obj) => trackingScreen.TrackingEvent(TrackingDisplay.TrackingEventType.nodeRemoved, obj);

    void InputTracking_nodeAdded(XRNodeState obj) => trackingScreen.TrackingEvent(TrackingDisplay.TrackingEventType.nodeAdded, obj);

    // Once you complete this module, we'll keep your Update function active
    // to drive the map display
    void Update ()
    {
        GenerateTrackingInfo();
        GatherDeviceButtonInput();
        UpdateOpponentSword();

        
        //trackingScreen.DebugInfo(
        //    string.Format("swrdPos X:{0} Y:{1} Z:{2} \n", lastControllerPosition.x, lastControllerPosition.y, lastControllerPosition.z) +
        //    string.Format("cpyPos X:{0} Y:{1} Z:{2} \n", leftOpponentSword.localPosition.x, leftOpponentSword.localPosition.y, leftOpponentSword.localPosition.z) +
        //    string.Format("swrdRot X:{0} Y:{1} Z:{2} \n", lastControllerRotation.x, lastControllerRotation.y, lastControllerRotation.z) +
        //    string.Format("cpyRot X:{0} Y:{1} Z:{2} \n", leftOpponentSword.localRotation.x, leftOpponentSword.localRotation.y, leftOpponentSword.localRotation.z)
        //    );
    }
    //use the player track pose driver info to move the left and right robots.
    private void UpdateOpponentSword()
    {

        //verify values and use the LastControllerPositon/Rotation as they are from the XRNodeState, you can trust them.
        //if no values try the InputTracking object on unspecified gameController.
        //Note Defaulting to GameController doesn't work in editor with a Rift.

        if (lastControllerPosition == Vector3.zero || lastControllerRotation == Quaternion.identity)
        {
            lastControllerPosition = InputTracking.GetLocalPosition(XRNode.GameController);
            lastControllerRotation = InputTracking.GetLocalRotation(XRNode.GameController);
           
        }


        //right robot
        opponentSword.localPosition = new Vector3(lastControllerPosition.x, lastControllerPosition.y,lastControllerPosition.z);
        opponentSword.localRotation = lastControllerRotation;
        opponentHead.rotation = new Quaternion(lastHeadRotation.x, -lastHeadRotation.y, lastHeadRotation.z, lastHeadRotation.w);
        //trackingScreen.DebugInfo(
        //       string.Format("swrdPos X:{0} Y:{1} Z:{2} \n", lastControllerPosition.x, lastControllerPosition.y, lastControllerPosition.z) +
        //       string.Format("cpyPos X:{0} Y:{1} Z:{2} \n", opponentSword.localPosition.x, opponentSword.localPosition.y, opponentSword.localPosition.z) +
        //       string.Format("swrdRot X:{0} Y:{1} Z:{2} \n", lastControllerRotation.x, lastControllerRotation.y, lastControllerRotation.z) +
        //       string.Format("cpyRot X:{0} Y:{1} Z:{2} \n", opponentSword.localRotation.x, opponentSword.localRotation.y,opponentSword.localRotation.z)
        //       );
        //left robot
        leftOpponentSword.localPosition = new Vector3(lastControllerPosition.x, lastControllerPosition.y, lastControllerPosition.z);
        leftOpponentSword.localRotation = new Quaternion(lastControllerRotation.x, lastControllerRotation.y, lastControllerRotation.z, lastControllerRotation.w);
        leftOpponentHead.rotation = new Quaternion(-lastHeadRotation.x, -lastHeadRotation.y, lastHeadRotation.z, lastHeadRotation.w);
    }

    private void FixedFrameUpdate()
    {
        //Five frame input window
        var targetFPS = 90;
        XRStats.TryGetFramePresentCount(out targetFPS);
        fpsText.text = string.Format("{0} FPS",targetFPS);
        //if (targetFPS > 0 && lastFrameCount >= inputFrameRate / targetFPS)
        //{
        //    //also executes trackingScreen.ControllerInfo
        //    RemoveFromInputBuffer();
        //    lastFrameCount = 0f;
        //}
        ////update are fixCounter
        //lastFrameCount += Time.fixedDeltaTime;
    }

    /// <summary>
    /// Read functions from UnityEngine.XR class to display onto the tracking screen.
    /// </summary>
    private void GenerateTrackingInfo()
    {
        var device = XRDevice.model;
        var spaceType = XRDevice.GetTrackingSpaceType();
        var hasController = false;
        //need defaults so IDE can shutup
        Vector3 headPosition = Vector3.zero;
        Quaternion headRotation = Quaternion.identity;
        Vector3 mainControllerPosition = Vector3.zero;
        Quaternion mainControllerRotation = Quaternion.identity;
        //grab all nodestates to safely tryGetPos and Rot
        InputTracking.GetNodeStates(nodeStates);
        for (int i = nodeStates.Count - 1; i >= 0; i--)
        {
            //limitation, only tracking right hand? need setting for user to transfer hands.
            //but on room space vr both hands are equal.

            //First use the node state and the more accurate TryGet Methods.
            //if any of the TryGetPosition or TGRotation return false 
            //fall back to the InputTracking.getPosition and IT.getRotation
            // took out check for left handers || nodeStates[i].nodeType == XRNode.LeftHand
            if (nodeStates[i].nodeType == XRNode.RightHand || nodeStates[i].nodeType == XRNode.GameController)
            {
                //we are tracking a controller
                hasController = nodeStates[i].tracked;
                var uid = nodeStates[i].uniqueID;
                mainController = InputTracking.GetNodeName(uid);
                if (!nodeStates[i].TryGetPosition(out mainControllerPosition))
                {
                    mainControllerPosition = InputTracking.GetLocalPosition(nodeStates[i].nodeType);
                }
                if(!nodeStates[i].TryGetRotation(out mainControllerRotation))
                {
                    mainControllerRotation = InputTracking.GetLocalRotation(nodeStates[i].nodeType);
                }
                lastControllerPosition = mainControllerPosition;
                lastControllerRotation = mainControllerRotation;
            }
            
            if (nodeStates[i].nodeType == XRNode.Head)
            {
                if(!nodeStates[i].TryGetPosition(out headPosition))
                {
                    //failed using accurate tracking so default to InputTracking
                    headPosition = InputTracking.GetLocalPosition(XRNode.Head);
                }
                if(!nodeStates[i].TryGetRotation(out headRotation))
                {
                    headRotation = InputTracking.GetLocalRotation(XRNode.Head);
                }
                lastHeadPosition = headPosition;
                lastHeadRotation = headRotation;
            }

        }

        var FPS = 0f;
        framesCount++;
        //this can be adjusted on the fly
        var renderViewportScale = XRSettings.renderViewportScale;
        if (XRStats.TryGetGPUTimeLastFrame(out FPS) )
        {
           frameTime += FPS;
           FPS = framesCount / frameTime;
           fpsText.text = (int)FPS + "FPS";
           if (frameTime > updateInterval)
           {
               framesCount = 0;
               frameTime = 0;
           }
        }
        trackingScreen.TrackingInfo(device, spaceType, headPosition, headRotation,
            hasController, mainControllerPosition, mainControllerRotation,
            FPS, renderViewportScale);

        
    }
    //No your inputs
    private const string VRTouchPadPush = "OCTouch.SecondaryThumbstick.Touch";
    private const string VRTouchPadPress = "OCTouch.SecondaryThumbstick.Press";
    private const string VRBackButton = "OCTouch.Button.Two.Press";
    private const string VRTouchPadX = "SecondaryThumbstick.Horizontal";
    private const string VRTouchPadY = "SecondaryThumbstick.Vertical";
    private const string VRTrigger = "OCTouch.SecondaryTrigger.Squeeze";
    private const string VRTakeScreenShot = "OCTouch.SecondaryGrip";
    void GatherDeviceButtonInput()
    {
        //Limited implementation of oculus Touch controller based on the oculus go.
        var joysticks = Input.GetJoystickNames();
        var controllerInput = new ControllerInput
        {
            Device = "",
            Trigger = 0.0f,
            Horizontal = Input.GetAxis(VRTouchPadX),
            Vertical = Input.GetAxis(VRTouchPadY),
            Touched = false,
            Pressed = false
        };
        //no joystick exit.
        if (joysticks.Length <= 0)
        {
            return;
        }

        if (mainController != null)
        //name gathered from GenerateTrackingInfo()
        {
            //name gathered from GenerateTrackingInfo()
            controllerInput.Device = mainController;
        }
        var msg = "";

        //Touch Pad X(Horizontal) - Axis 4 - Joystick 4th Axis
        //right and left
        //try oculus GO
        var xPosGO = Input.GetAxis("TouchPad.Horizontal");
        if (xPosGO > defaultStickDeadZone || xPosGO < defaultStickDeadZone) {
            controllerInput.Horizontal = xPosGO;
        }


        //Touch Pad Y(Vertical) - Axis 5 - Joystick 5th Axis
        //down and up
        //try oculus GO
        var yPosGO = Input.GetAxis("TouchPad.Vertical");
        if (yPosGO > defaultStickDeadZone || yPosGO < defaultStickDeadZone)
        {
            controllerInput.Vertical = yPosGO;
        }
        //For each button press or joystick movement set one of the following.
        //device name, bool isTPTouched, bool isTPPressed, float trigger, float tpHorizontal, float tpVertical
        //Trigger - Axis 3 - Joystick 3rd Axis
        if (Input.GetAxis(VRTrigger) > defaultAxisDeadZone || Input.GetAxis("OCG.Trigger") > defaultAxisDeadZone)
        {
            controllerInput.Trigger = Input.GetAxis("OCTouch.SecondaryTrigger.Squeeze") > defaultAxisDeadZone ? Input.GetAxis(VRTrigger) : Input.GetAxis("OCG.Trigger");
            
        }

        if (Input.GetAxis(VRTakeScreenShot) > defaultAxisDeadZone )
        {
            var now = DateTime.Now.ToString("dd-mm-yy-HH_mm_ss");
            msg = "took screenshot";
            trackingScreen.DebugInfo(msg);
            trackingScreen.TakeScreenshot("manualsnap" + now);
        }

        //B Button - Button.Two - joystick button 1 on Oculus touch
        //Back Button -Button 2 - joystick button 1 on Oculus GO
        controllerInput.Pressed |= (Input.GetButton(VRBackButton) || Input.GetButton("OCG.Button.Two"));
        

        //oculus Go Touch Pad Push - Button 9 - joystick button 9
        controllerInput.Pressed |= (Input.GetButton(VRTouchPadPress) || Input.GetButton("OCG.TouchPad.Pressed"));
        
        controllerInput.Touched = Input.GetButton(VRTouchPadPush) || (Math.Abs(controllerInput.Horizontal) > 0.0f && Math.Abs(controllerInput.Vertical) > 0.0f );
        if (Input.GetButton(VRTouchPadPush))
        {
            Debug.Log("touching thumbstick");
        }
        
        //caused crap performacne
        //AddToInputBuffer(currentInput);
        trackingScreen.ControllerInfo(controllerInput.Device, controllerInput.Touched, controllerInput.Pressed,
            controllerInput.Trigger, controllerInput.Horizontal, controllerInput.Vertical);
    }
}
struct ControllerInput
{
    public string Device;
    public bool Pressed;
    public bool Touched;
    public float Trigger;
    public float Horizontal;
    public float Vertical;
}
