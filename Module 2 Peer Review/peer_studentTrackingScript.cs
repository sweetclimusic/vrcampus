﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.UI;
using vrCampusCourseware;

public class studentTrackingScript : MonoBehaviour {

    [Header("Courseware")]
    [SerializeField]
    TrackingDisplay trackingScreen;

    [SerializeField] TrackingDisplay trackingDisplay;

    [SerializeField] Transform opponentSword;
    [SerializeField] Transform opponentSwordLeft;

    //Input Manager Names
    //GRADING NOTE: EDIT THESE STRING VALUES TO MATCH YOUR OWN OR CREATE WITH THE SAME NAMES IN THE INPUT MANAGER
    private const string VRTouchPadPush = "VRTouchPadPush";
    private const string VRBackButton = "VRBackButton";
    private const string VRTouchPadX = "VRTouchPadX";
    private const string VRTouchPadY = "VRTouchPadY";
    private const string VRTrigger = "VRTrigger";

    // the code below allows your script to know what room is currently being used
    // for some rooms you want the code you write to always work
    // in other rooms, your code should only do things if you're in a specific room
    switchRooms.roomType curRoom = switchRooms.roomType.Study;
    public void switchRoom(switchRooms.roomType type)
    {
        curRoom = type;
    }

    // The tracking module shows you how to work with most of the low level info
    // you can get from the XR API. Get the raw information from the XR APIs and
    // pass it to the tracking display to help log progress for your peer review.

    // Once you complete this module, we'll keep your Update function active
    // to drive the map display no matter what module you're in

    void Start()
    {
        InputTracking.nodeAdded += InputTrackingOnNodeAdded;
        InputTracking.nodeRemoved += InputTracking_nodeRemoved;
        InputTracking.trackingAcquired += InputTracking_trackingAcquired;
        InputTracking.trackingLost += InputTracking_trackingLost;
    }

    private void InputTracking_trackingLost(XRNodeState obj)
    {
        trackingDisplay.TrackingEvent(TrackingDisplay.TrackingEventType.trackingLost, obj);
    }

    private void InputTracking_trackingAcquired(XRNodeState obj)
    {
        trackingDisplay.TrackingEvent(TrackingDisplay.TrackingEventType.trackingAcquired, obj);
    }

    private void InputTracking_nodeRemoved(XRNodeState obj)
    {
        trackingDisplay.TrackingEvent(TrackingDisplay.TrackingEventType.nodeRemoved, obj);
    }

    private void InputTrackingOnNodeAdded(XRNodeState xrNodeState)
    {
        trackingDisplay.TrackingEvent(TrackingDisplay.TrackingEventType.nodeAdded, xrNodeState);
    }

    void Update()
    {
        string DeviceModel = XRDevice.model;
        TrackingSpaceType TrackingType = XRDevice.GetTrackingSpaceType();

        //Initial values
        Vector3 HeadsetPos = new Vector3(0, 0, 0);
        Quaternion HeadsetRot = Quaternion.identity;
        bool controllerRPresent = false;
        bool controllerLPresent = false;
        bool controllerGPresent = false;
        Vector3 controllerPo = new Vector3(0, 0, 0);
        Quaternion controllerRot = Quaternion.identity;
        float FPS = 0;
        float viewportScale = 0;

        List<XRNodeState> nodeStates = new List<XRNodeState>();
        InputTracking.GetNodeStates(nodeStates);

        XRNodeState centerEyeNode = nodeStates.FirstOrDefault(n => n.nodeType == XRNode.CenterEye);
        XRNodeState controllerRNode = nodeStates.FirstOrDefault(n => n.nodeType == XRNode.RightHand);
        XRNodeState controllerLNode = nodeStates.FirstOrDefault(n => n.nodeType == XRNode.LeftHand);
        XRNodeState controllerGNode = nodeStates.FirstOrDefault(n => n.nodeType == XRNode.GameController);

        if (centerEyeNode.nodeType == XRNode.CenterEye)
        {
            Vector3 hp;
            if (centerEyeNode.TryGetPosition(out hp))
            {
                HeadsetPos = hp;
            }
        
            Quaternion hr;
            if (centerEyeNode.TryGetRotation(out hr))
            {
                HeadsetRot = hr;
            }
        }

        if (controllerRNode.nodeType == XRNode.RightHand)
        {
            controllerRPresent = true;

            Vector3 cp;
            if (controllerRNode.TryGetPosition(out cp))
            {
                controllerPo = cp;
            }

            Quaternion cr;
            if (controllerRNode.TryGetRotation(out cr))
            {
                controllerRot = cr;
            }
        }

        if (controllerLNode.nodeType == XRNode.LeftHand)
        {
            controllerLPresent = true;
        }

        if (controllerGNode.nodeType == XRNode.GameController)
        {
            controllerGPresent = true;
        }

        trackingDisplay.DebugInfo("Controllers " + (controllerRPresent ? "Right " : "") + (controllerLPresent ? "Left " : "") + (controllerGPresent ? "Game " : ""));

        FPS = 1.0f / Time.deltaTime;

        viewportScale = XRSettings.renderViewportScale;

        trackingDisplay.TrackingInfo(DeviceModel, TrackingType, HeadsetPos, HeadsetRot, controllerRPresent, controllerPo, controllerRot, FPS, viewportScale);

        var vrTouchPadPush = Input.GetButton(VRTouchPadPush);
        var vrBackButton = Input.GetButton(VRBackButton);
        var vrTouchPadX = Input.GetAxis(VRTouchPadX);
        var vrTouchPadY = Input.GetAxis(VRTouchPadY);
        var vrTrigger = Input.GetAxis(VRTrigger);

        trackingDisplay.ControllerInfo(DeviceModel, (vrTouchPadX != 0.0f) || (vrTouchPadY != 0.0f), vrTouchPadPush, vrTrigger, vrTouchPadX, vrTouchPadY);

        PositionOpponentSwords();
    }

    private void PositionOpponentSwords()
    {
        Vector3 pos = InputTracking.GetLocalPosition(XRNode.RightHand);
        Quaternion rot = InputTracking.GetLocalRotation(XRNode.RightHand);

        opponentSword.transform.localPosition = pos;
        opponentSword.transform.localRotation = rot;

        if (opponentSwordLeft != null)
        {
            opponentSwordLeft.transform.localPosition = pos;
            opponentSwordLeft.transform.localRotation = rot;
        }
    }
}
